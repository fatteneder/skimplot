IDX = ARG1
eval skimplot_update_status

### plot
plot $DATA1 i @IDX u 1:2 w l title skimplot_status[1]

### surface plot
# splot $DATA1 i @IDX u 1:2:3 w pm3d title skimplot_status[1]

### multiplot
# set multiplot layout 2,1
# plot $DATA1 i @IDX u 1:2 w l title skimplot_status[1]
# plot $DATA2 i @IDX u 1:2 w l title skimplot_status[2]
# unset multiplot

### surface multiplot
# set multiplot layout 1,2 title
# splot $DATA1 i @IDX u 1:2:3 w pm3d title skimplot_status[1]
# splot $DATA2 i @IDX u 1:2:3 w pm3d title skimplot_status[2]
# unset multiplot
