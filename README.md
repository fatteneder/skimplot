# Skimplot

Skimplot allows you to conviniently skim through your data sets interactively
using Gnuplot. It also offers macros to convert your plots into a GIF or a video.

This script was inspired by the book [Gnuplot in Action](https://www.manning.com/books/gnuplot-in-action-second-edition).

## Installation

Requirements
* [Gnuplot](http://www.gnuplot.info/) >= 5
* [FFmpeg](https://ffmpeg.org/) >= ??? (for export to .mp4)

```
$ git clone https://gitlab.com/fatteneder/skimplot.git
```

## Usage

Start Gnuplot
```
$ cd skimplot
$ gnuplot
```
and load your data `datafile.gp` by typing
```
gnuplot> call 'skimplot.gp' 'datafile.gp'
```
A plot of the first data set should appear. While focusing the plot
window you can hit `Alt-H` to get a list of available key binds to skim
through your data file.

Example data files can be found inside `example/`.

### Modify the plot

Adjusting the plot can be done by editing `plot.gp` to your needs. 
No need to restart `gnuplot`, just save `plot.gp` and continue 
skimming where you stopped.

#### Header
The `plot.gp` file should contain the following lines at the top
```
IDX = ARG1
eval skimplot_update_status
```
The first line is an abbreviation for the current data index,
the second line updates the status info before it is used in the plot title.

#### Single plot
For skimming through 1D data sets put the following line into the `plot.gp` file:
```
plot $DATA1 i @IDX u 1:2 w l title skimplot_status[1]
```
> The plot command must always start with `plot $DATA1 i @IDX ...`!

#### Single surface plot
Adjust `plot.gp` to something like
```
splot $DATA1 i @IDX u 1:2:3 w pm3d title skimplot_status[1]
```
> The plot command must always start with `splot $DATA1 i @IDX ...`! 
Remember to use `splot` instead of `plot` for surface plots.

The variable `skimplot_status` is an array of strings with information
about the current block of data in the file. 

#### Multiple data files
You can load multiple data sets from different files, simply put them one 
after another, e.g.
```
gnuplot> call 'skimplot.gp` `data1.gp` `data2.gp` ...
```
The data will be loaded into the variables `$DATA1`, `$DATA2`.
You can use them in `plot.gp` for `plot/splot` commands like
```
plot $DATA1 i @IDX u 1:2 w l title skimplot_status[1], \
    $DATA2 i @IDX u 1:2 w l title skimplot_status[2]
```

#### Multiple (surface) plots 
You can also skim through multiple plots side by side, simply make use of
Gnuplot's `multiplot` mode. 

For 1D plots use something like
```
set multiplot layout 2,1
plot $DATA1 i @IDX u 1:2 w l title skimplot_status[1]
plot $DATA2 i @IDX u 1:(-column(2)) w l title skimplot_status[2]
unset multiplot
```
and for surface plots use
```
set multiplot layout 1,2 
splot $DATA1 i @IDX u 1:2:3 w pm3d title skimplot_status[1]
splot $DATA2 i @IDX u 1:2:3 w pm3d title skimplot_status[2]
unset multiplot
```
Compared to a single (surface) plot, we have only added a `set multiplot` 
(together with a layout option (2x2 grid) and a title) and a `unset multiplot` 
line at the beginning and end of a block of `plot/splot` commands.

> Again, note that each plot command must start with 
`plot/splot $DATA1 i @IDX ...`!

### Generate GIF and Video

To export the data set as a `.gif` file type 
```
gnuplot> @skimplot_export_gif
```
and for a `.mp4` file type
```
gnuplot> @skimplot_export_mp4
```


_Enjoy!_


## Issues

### TODO
* ~~Provide better example data sets.~~
* ~~Gnuplot warning at start: something is wrong with the data file and 
`readdata.gp`.~~
* Add parameter to set or make an estimate for the frame rate of the .mp4 
output.
* Add info about data file layout.
* Figure out minimum version of FFmpeg.
* Add a command to jump to a specific data block in file.

### Bugs
1. ~~If `Alt-F` or `Alt-B` were pressed multiple times, then pressing `Alt-S`
once does not stop skimming.~~
2. ~~While skimming forwards or backwards the plot window continuously
reappears if it is closed. Expected behavior: skimming should stop and window
should not appear again.~~
3. ~~If multiple terminals are used, then for stepping through data sets with
`Al-N`, `Alt-P` one must press and release the `Alt` key for each step.
Expected behavior: The same behavior as for a single plot window is expected, that is, hold down `Alt` and press the second key.~~
Resolved by removing automatic skim commands.
4. ~~Key bindings are only registered if the last plot window (the
last one specified in `plot.gp`) is focused.~~
We do not support multiple plot windows, use `multiplot` mode instead.
5. ~~Plot window is constantly raised, this prevents doing other stuff while
skimming.~~
6. ~~While in skimming loop, the command line does not respond to any commands.
Expected behavior: register key binds or allow to enter commands manually.~~
Removed automatic skimming because mp4 export is now available.
7. Number of data blocks is incorrect, hence, parts of the data files are not
plotted.
8. If `ffmpeg` is not available on system, then the command 
`@skimplot_export_mp4` outputs a system message about missing `ffmpeg`, but
finishes with message `Video was saved to ...`.
Expected behavior: Abort exporting when `ffmpeg` is not found.


### Features
1. [X] Support loading data from multiple files.
2. [X] Support column headers.
3. [X] Support plotting from multiple sources/data files.
4. [X] Support multiplots (~~wait for https://sourceforge.net/p/gnuplot/bugs/2276/
to be released~~).
5. [X] Print status for each data set, e.g.
```
skimplot> [10/50], T = ...
```
~~Status is now plotted in legend together with column header.~~
Added array `skimplot_status` that holds string with info about current 
data block for each file loaded.

6. [X] Export plot as .mp4 file.
7. [ ] Allow for user specified `plot.gp` file
