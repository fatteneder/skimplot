#!/usr/bin/env bash

#######################################################################
#                               SYNTAX                                #
#######################################################################

# Skimplot is called as
#   call "skimplot.gp" <plot command>
#
# The following <plot commands> are supported
#   1. Plot single line
#     call "skimplot.gp" "file1"
#   2. Plot line for each file
#     call "skimplot.gp" "file1, file2, file3"
#   3. Same as 2. but gather files by glob expansion
#     call "skimplot.gp" "file*"
#   4. Plot result of some computation
#     call "skimplot.gp" "fileA**2 * (log(fileB) + 1.0)"
#   5. Same as 4 but with globbing
#     call "skimplot.gp" "([fileA*]**2 * (log([fileB*]) + 1.0)"
#   6. Plot select columns
#     call "skimplot.gp" "file$2"
#   7. Plot data from one file against column of another file
#     call "skimplot.gp" "fileA vs. fileB"
#     Not sure if we should implement this, because gnuplot does not provide
#     syntax for this. One would use something like `paste fileA fileB`.
#   8. Same as 6. and 7. but with globbin patterns
#     call "skimplot.gp" "[fileA*]$2"
#     call "skimplot.gp" "[fileA*] vs. [fileB*]"


#######################################################################
#                                SETUP                                #
#######################################################################

thisdir="$(dirname $(realpath ${BASH_SOURCE[0]}))"
tmpdir="$thisdir/../tmp"
script="$tmpdir/script.gp"
variables="$tmpdir/variables.txt"
inputs="$tmpdir/inputs.txt"

[[ ! -d "$tmpdir" ]] && mkdir "$tmpdir"

source "$thisdir/utils.sh"

# be careful with cmd and prevent expanding any glob patterns
# by using double quotes
cmd="$1"
opts="$2"
cmd=($(echo "$cmd"))

cat << EOF > $script
IDX = ARG1
# eval skimplot_update_status

plot \\
EOF

vars=()
files=("${cmd[@]}")
nfiles="${#files[@]}"
for idx in  "${!files[@]}"; do
  vars[$idx]="DATA_$idx"
  file="${files[$idx]}"
  str="   \$${vars[$idx]} i @IDX w l"
  [[ $idx < $(($nfiles-1)) ]] && str="$str, \\"
  # echo $idx $(($nfiles-1))
  # echo "$str"
  echo "$str" >> $script
done

echo_array "${vars[@]}" > "$variables"
echo_array "${files[@]}" > "$inputs"
