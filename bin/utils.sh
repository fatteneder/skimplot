#!/usr/bin/env bash


find_enclosed() {
  lhs="$1"
  rhs="$2"
  src="$3"
  str=("$(echo $src | awk -v RS=$lhs -v FS=$rhs 'NR>1{print $1}')")
  echo "$str"
}
# TODO Check why output contains line breaks.
### example
# test="string1 [*.gp] string3 [string4]"
# str=$(find_enclosed '[' ']' "$test")
# echo "Input: $test"
# echo "Expected: *.gp string4"
# echo "Output: ${str[@]}"


count_char() {
  char="$1"
  str="$2"
  n=$(echo "$str" | tr -cd "$char" | wc -m)
  echo $n
}
### example
# test="string1 [*.gp] string3 [string4]"
# bropen=$(count_char "[" "$test")
# brclose=$(count_char "]" "$test")
# echo "Input: $test"
# echo "Expected: 2 2"
# echo "Output: $bropen $brclose"


echo_array() {
  printf "%s\n" "$@"
}
### example
# test="string1 string2 string3"
# test=($(echo "$test"))
# echo "Input: $test"
# echo "Expected:"
# echo "string1"
# echo "string2"
# echo "string3"
# echo "Output:"
# echo_array "${test[@]}"
