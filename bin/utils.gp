string_to_array(str, var) = sprintf( " \
  s = \"%s\"; n = words(s); \
  array %s [n]; \
  do for [i=1:n] {; \
    %s[i] = word(s, i); \
  }; \
  undefine s n; \
", str, var, var)

print_array(var) = sprintf( " \
  n = |%s|; \
  do for [i=1:n] {; \
    print(%s[i]); \
  }; \
  undefine n; \
", var, var)


# # test array operation
# files = system("ls -1 *.md")
# eval string_to_array(files, "myvar")
# eval print_array("myvar")

# TODO add variable divisors
# use shell command: printf '=%.0s' {1..100}
print_divisor = sprintf("print \
\"--------------------------------------------------------------------------\"")

get_extension(filepath, extension) = sprintf( " \
  %s = system(\"filepath=\\\"%s\\\"; echo \\\"\${filepath#*.}\\\"\"); ", \
  extension, filepath)

get_filename(filepath, filename) = sprintf( " \
  %s = system(\"filepath=\\\"%s\\\"; echo \\\"\${filepath%%.*}\\\"\"); ", \
  filename, filepath)

isnan(x) = (x == x) ? 0 : 1

length_header(filename, result) = sprintf( " \
  linenr = system(\"bash \".skimplot_dir.\"/bin/scan_header.sh \\\"%s\\\"\") + 0; \
  linenr = int(linenr); \
  if (linenr > 0) {; \
    linenr = linenr - 1; \
  }; \
  %s = linenr; \
  ", filename, result)
