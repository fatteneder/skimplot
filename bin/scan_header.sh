#!/usr/bin/env bash

filename=$1

linenr=0
while read line; do
  if [[ "${line[0]:0:1}" != "\"" ]]; then
    break
  fi
  linenr=$((linenr+1))
done < $filename

echo $linenr
