# Current approach

Skimplot is written as a raw gnuplot script with the following features
in mind:
  - Full access to gnuplots command line interface: 
    One can adjust the current plots using the rich feature 
    set of gnuplot.
  - Portability: Only dependency is a Gnuplot installation which
    is fairly common on Unix systems.
  - Easy to setup: Download the repository, start gnuplot, done.
  - Speed: Gnuplot is one of the fastest and most responsive plotting
    programs out there - full stop.

Issues:
  - (minor) Inconvinience in calling the script: One always has to start
    a gnuplot session and then run the script explicitly, see below
    for alternative approaches.
    Actually, I think this is just the price we have to pay for having
    the gnuplot command line available.
  - Clumsy interaction with the plot: 
    It is not possible to add new buttons or text fields to the plotting
    window. There are some standard keybindings for zooming and 
    moving plots. IDK if more advanced features could be implemented 
    (or are even needed?).
    Idea: Implement custom buttons as overlay text fields on plot 
    and use mouser interactions on given positions to trigger events.
  - Gnuplot scripting is just ugly, cf. skimplot.gp.
    We could outsource parts of this into a bash script.
    However, then the problem is that reading return values from a
    bash script only works nicely for strings. Reading into an array
    is not trivially possible and would require more ugly gnuplot
    scripting.

# Previous Ideas

Here is a list of approaches I have come up with and abondoned already.

- "Make skimplot a bash script and call gnuplot in batch mode"
  This would simplify argument parsing and would allow for nicer
  interaction directly from the terminal, e.g. no need to start
  gnuplot explicitly and then type something like 
  `call "skimplot.gp" "file1.dat" ...`. The emphasize is on nicer,
  because the above syntax is in principle ok, but if one forgets
  to put in quotes then gnuplot gives cryptic errors.
  The idea of a bash script would be to make the skimplot only
  accept a plotting script (currently `plot.gp`) and the input files
  from where to read. The bash script would then call skimplot with
  `gnuplot -c "skimplot.gp" "file1.dat" ...` and will wait till
  it finishes to return. If we would add a `pause mouse close` 
  to `skimplot.gp` then the whole project would return to the terminal
  when the last plot is closed. All of this would make skimplot to 
  look like a standalone program.

  Problems: We would like to maintane the possibiltiy to run gnuplot
  functions while skimming plots, e.g. `set xlabel "x"`.
  Actually one could ask for command inputs with a 
  `system("read var; echo $var")` call. However, if we run gnuplot in
  batch mode (-e option above) then the only way to interact with it
  is to use keybindings registered by the active plotting window,
  like we already do it with `bind allwindows 'alt-E' "print('hello')"`
  (note that the actual keybinding must be enclosed in single quotes).
  Unfortunately, calling `system` from a `bind` does not work, e.g.
  nothing I type is processed and hitting return does not finish the
  `read` call. This is a bit confusing, because other commands like
  `echo "hello"` do still work normally.

- "Make skimplot a bash script and start a subprocess with `coproc`"
  This could be done with
  ```
  bash-5.1$ coproc mygp { gnuplot; }
  [1] 26573
  bash-5.1$ echo "print('hello')" >&${mygp[1]}
  hello
  ```
  In this way we could keep listening to the terminal with a
  read command and forward everything to gnuplot. This is nice, because
  it allows to forward any user input to gnuplot easily.
  The only problem I see is that calling interactive functions does not
  work out of the box, e.g.
  ```
  bash-5.1$ echo "print('help')" >&${mygp[1]}
  ```
  will only show part of the help page and scrolling or searching through
  it is not possible. Also once the help page is called returning to
  normal gnuplot input is difficult, sending `\r` (ASCII return)
  leaves the interactive mode, but also prints a warning.

- "Write a cpp program with gnuplot-iostream or gnuplot-cpp"
  Not worth the effort and I think we run into the same problems.
