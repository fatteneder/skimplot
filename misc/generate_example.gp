thisfile = ARG0
thisfilename = "generate_example.gp"
# get directory of this file
pwd = system("pwd -L") # working directory gnuplot session
idx_thisfilename = strstrt(thisfile, thisfilename)
thisfiledir = pwd."/".thisfile[*:idx_thisfilename-1]
exampledir = thisfiledir."../examples/"

generate_datafile(fname) = sprintf( " \
  print \"    Generating %s ...\", \
  system(\"[ -f \\\"%s\\\" ] && rm %s\"); \
  system(\"touch %s\");", \
  fname, fname, fname)

if (1) {
  filename    = exampledir."sine1d.gp"
  eval generate_datafile(filename)
  set print filename append

  nt = 100; tstart = 0.0; tend = 1.0; dt = (tend - tstart) / nt
  nx = 150;  xmin = -0.5; xmax = 0.5; dx = (xmax - xmin) / nx
  do for [ti=0:nt-1] {
    t = tstart + ti * dt
    print sprintf("\"Time = %f", t)
    do for [xi=0:nx-1] {
      x = xmin + xi * dx
      f = sin(2*pi*(x - t))
      print x, f
    }
    print "\n"
  }

  unset print
}

if (1) {
  filename    = exampledir."cosine1d.gp.1"
  eval generate_datafile(filename)
  set print filename append

  nt = 100; tstart = 0.0; tend = 1.0; dt = (tend - tstart) / nt
  nx = 150;  xmin = 0.0; xmax = 1.0; dx = (xmax - xmin) / nx
  do for [ti=0:nt-1] {
    t = tstart + ti * dt
    print sprintf("\"Time = %f", t)
    do for [xi=0:nx-1] {
      x = xmin + xi * dx
      f = cos(2*pi*(x - t))
      print x, f
    }
    print "\n"
  }

  unset print

  filename    = exampledir."cosine1d.gp.2"
  eval generate_datafile(filename)
  set print filename append

  nt = 100; tstart = 0.0; tend = 1.0; dt = (tend - tstart) / nt
  nx = 150;  xmin = 1.0; xmax = 2.0; dx = (xmax - xmin) / nx
  do for [ti=0:nt-1] {
    t = tstart + ti * dt
    print sprintf("\"Time = %f", t)
    do for [xi=0:nx-1] {
      x = xmin + xi * dx
      f = cos(2*pi*(x - t))
      print x, f
    }
    print "\n"
  }

  unset print
}

if (1) {
  filename    = exampledir."seawave1d.gp"
  eval generate_datafile(filename)
  set print filename append

  # t grid
  nt = 100; tstart = 0.0; tend = 1.0; dt = (tend - tstart) / nt
  # x grid
  nx = 150;  xmin = -0.5; xmax = 0.5; dx = (xmax - xmin) / nx

  # parameters
  A = 0.95

  do for [ti=0:nt-1] {
    t = tstart + ti * dt
    print sprintf("\"Time = %f", t)
    do for [xi=0:nx-1] {
      x = xmin + xi * dx
      f = - A * pi * cos(2.0 * pi * (x - t)) / \
          (3.0 * (1.0 + A * sin(2.0 * pi * (x - t)))**(1.0/3.0))
      print x, f
    }
    print "\n"
  }

  unset print
}


if (1) {
  filename    = exampledir."pebble2d.gp"
  eval generate_datafile(filename)
  set print filename append

  # example taken from Gnuplot in Action, Philip K. Janet, 2nd Edition, 2016
  # Listing 11.6, p. 255
  nt = 100; tstart = 0.0; tend = 2*pi; dt = (tend - tstart) / nt
  nx = 40;  xmin = -10.0; xmax = 10.0; dx = (xmax - xmin) / nx
  ny = 40;  ymin = -10.0; ymax = 10.0; dy = (ymax - ymin) / ny
  do for [ti=0:nt-1] {
    t = tstart + ti * dt
    print sprintf("\"Time = %f", t)
    do for [xi=0:nx-1] {
      x = xmin + xi * dx
      do for [yi=0:ny-1] {
        y = ymin + yi * dy
        f = exp(-0.2*sqrt(x**2+y**2))*cos(sqrt(x**2+y**2) - t)
        print x, y, f
      }
      print ""
    }
    print ""
  }

  unset print
}
