######### variables
skimplot_idx            = 0
skimplot_direction      = 1     # skim forwards = 1, skim backwards = -1
skimplot_blocks         = 1e100 # smallest number of data blocks found
                                # among all loaded files

skimplot_plot(index) = sprintf( "call skimplot_dir.'/tmp/script.gp' %i", index)

skimplot_load_file(file, var) = sprintf( " \
  file = \"%s\"; \
  eval length_header(file, \"len\"); \
  if (len > 0) {; \
    load '< echo \"\\$%s << EOD \" & awk \"(NR>'.len.'){print;}\" \"'.file.'\"'; \
  } else {; \
    load '< echo \"\\$%s << EOD \" & cat \"'.file.'\"'; \
  }; \
  set key auto columnheader; \
  stats $%s name \"%s\" nooutput; \
  %s_blocks = %s_blocks - 1; \
  if (%s_blocks < skimplot_blocks) {; \
    skimplot_blocks = %s_blocks; \
  }; \
  print \"     I found \".%s_blocks.\" data blocks in \" . file; \
  undefine file; \
  ", \
  file, var, var, var, var, var, var, var, var, var)

skimplot_load = sprintf( " \
  @print_divisor; \
  do for [idx = 1:|vars|] {; \
    input = inputs[idx]; \
    var = vars[idx]; \
    eval skimplot_load_file(input, var); \
  }; \
  @print_divisor; \
  " )

skimplot_increment_index(increment) = sprintf( " \
  skimplot_idx = skimplot_idx + %i; \
  if (skimplot_idx >= skimplot_blocks) {; \
    skimplot_idx = skimplot_idx - skimplot_blocks; \
  }; \
  if (skimplot_idx < 0) {; \
    skimplot_idx = skimplot_blocks + skimplot_idx; \
  }; ", \
  increment)

bind allwindows 'alt-L' " \
  print \"skimplot> Reloading data ...\"; \
  eval skimplot_load; \
"

bind allwindows 'alt-.' " \
  eval skimplot_increment_index(5); \
  eval skimplot_plot(skimplot_idx); \
"

bind allwindows 'alt-,' " \
  eval skimplot_increment_index(-5); \
  eval skimplot_plot(skimplot_idx); \
"

bind allwindows '.' " \
  eval skimplot_increment_index(1); \
  eval skimplot_plot(skimplot_idx); \
"

bind allwindows ',' " \
  eval skimplot_increment_index(-1); \
  eval skimplot_plot(skimplot_idx); \
"

bind allwindows 'Button1' " \
  eval skimplot_increment_index(10); \
  eval skimplot_plot(skimplot_idx); \
"

bind allwindows 'Button3' " \
  eval skimplot_increment_index(-10); \
  eval skimplot_plot(skimplot_idx); \
"

bind allwindows 'Close' " \
  print \"Have a nice day!\"; \
"

bind allwindows 'alt-H' " \
  print \"The following short cuts are provided by Skimplot:\"; \
  print \"    alt-H     ...   show this help\"; \
  print \"        .     ...   plot next data set\"; \
  print \"        ,     ...   plot previous data set\"; \
  print \"    alt-.     ...   plot next data set\"; \
  print \"    alt-,     ...   plot previous data set\"; \
"

# # TODO Check whether making .png outputs is more efficient
skimplot_export_gif(filename) = sprintf( " \
  _path_gif = skimplot_dir.\"/output/%s\"; \
  eval print_divisor; \
  print \"\n    Generating gif output ...\n\"; \
  eval print_divisor; \
  set terminal gif animate delay SKIMPLOT_FPS; \
  set output _path_gif.\".gif\"; \
  do for [idx = 0:skimplot_blocks-1] {; \
    eval skimplot_plot(idx); \
  }; \
  print \"\n\"; \
  unset output; \
  eval SKIMPLOT_TERMINAL; \
  eval print_divisor; \
  print \"\n    Output was written to '\"._path_gif.\".gif'\n\"; \
  undefine _path_gif; \
  ", filename)

# # TODO use -t ffmpeg option to set total time of mp4 video
# # see https://stackoverflow.com/a/29435809
skimplot_export_mp4(filename) = sprintf( " \
  _file_mp4 = \"%s\"; \
  _path_mp4 = skimplot_dir.\"/output/\"._file_mp4; \
  eval skimplot_export_gif(_file_mp4); \
  eval print_divisor; \
  print \"\n    Converting gif to mp4 ...\n\"; \
  eval print_divisor; \
  system(\"ffmpeg -i \"._path_mp4.\".gif -movflags faststart \
  -pix_fmt yuv420p \"._path_mp4.\".mp4\"); \
  eval print_divisor; \
  print \"\n    Video was saved to '\"._path_mp4.\".mp4'\n\"; \
  undefine _file_mp4 _path_mp4; \
  eval skimplot_clean_gifs; \
  ", filename)

skimplot_clean_gifs = sprintf( " \
  _cmd = \"rm \".skimplot_dir.\"/output/*.gif\"; \
  system(_cmd); \
  undefine _cmd; \
  ")


if (ARGC == 1) {
  plotcmd  = ARG1
} else {
  print "Usage: call 'skimplot.gp' '<plotcmd>'"
  exit
}

skimplot_dir = system(sprintf("realpath \"%s\" | xargs dirname", ARG0))
system(sprintf("bash bin/parse.sh \"%s\"", plotcmd))

load skimplot_dir."/bin/utils.gp"
load skimplot_dir."/skimplotrc"

str_vars = system("cat tmp/variables.txt")
str_inputs = system("cat tmp/inputs.txt")

eval string_to_array(str_vars, "vars")
eval string_to_array(str_inputs, "inputs")
eval skimplot_load

if (!exists("SKIMPLOT_TERMINAL")) {
  SKIMPLOT_TERMINAL = "set terminal qt raise font ',12'"
}
eval SKIMPLOT_TERMINAL
eval SKIMPLOT_CFG

eval skimplot_plot(skimplot_idx)
print "     Press Alt-H on any active plot window to get help."

# TODO Write function to set axis limits based on loaded data.
